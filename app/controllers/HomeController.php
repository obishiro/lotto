<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function index()
	{
		return View::make('login');
	}
	public function postLogin() 
	 { 
		$rules = array(
			'userid'	=> 'required',
			'password'	=> 'required'

			);
		$validator = Validator::make(Input::all(), $rules);

    if ($validator->fails())
    {
        return Redirect::to('/')->withErrors($validator)->withInput();
     }else{

      

     		if(Auth::attempt(array('userid'=>Input::get('userid'),'password'=>Input::get('password')))) {
     			  
     			// Session::put('pid', '1');
     			$count_check = Period::where('pdate',date('Y-m-d'))->count();
     			if($count_check > 0){
     			$check = Period::where('pdate',date('Y-m-d'))->first();
     				Session::put('pid', $check->id);
     			}
     		
     			 return Redirect::to('home/dashboard');

     		}else{
     				$msg ="Error";
     			return  Redirect::to('/')->with($msg);
     			 
     		}
     		 
     }
    
	}
	public function getLogout()
	{
		Auth::logout();
		Session::forget('list');
		Session::forget('pid');
		Session::forget('r2');
		Session::forget('r3');
		Session::forget('c2');
		Session::forget('c3');
		Session::forget('t2');
		Session::forget('t3');
		return Redirect::to('/');
	}


	public function getDashboard()
	{
		if(Auth::check()){
				if(Auth::user()->usertype == 1)
		  	{
	 		 
 
		  $total2 = DB::table('tb_number')
		  	->where('period',Session::get('pid'))
		  	->where('length','2')->sum('price');
		  $total3 = DB::table('tb_number')
		  	->where('period',Session::get('pid'))
		  	->where('length','3')->sum('price');
		  $total = DB::table('tb_number')
		  	 ->where('period',Session::get('pid'))
		    	->sum('price');
		   $user = DB::table('users')->count();
		   $count_max_buy2 = Number::where(array('length'=>'2','period'=>Session::get('pid')))->count();
		   if($count_max_buy2 <=0)
		   {
		   	$max_buy2 ='0';
		   	$sum_max_buy2 = '0';
		   }else{
		   $max_buy2_data = DB::table('tb_number')
		   ->select(DB::raw('number,COUNT(id) as row'))
		   ->where('period',Session::get('pid'))
		   ->where('length','2')
		   ->groupBy('number')
		     ->skip(0)->take(1)
		   ->orderByRaw(DB::raw('COUNT(id) desc'))->first();
		   $max_buy2 = $max_buy2_data->number;
		   $sum_max_buy2 = Number::where('period',Session::get('pid'))
		   ->where('length','2')->where('number',$max_buy2_data->number)->sum('price');
		}
		 $count_max_buy3 = Number::where(array('length'=>'3','period'=>Session::get('pid')))->count();
		   if($count_max_buy3 <=0)
		   {
		   	$max_buy3 ='0';
		   	$sum_max_buy3 = '0';
		   }else{
		   $max_buy3_data = DB::table('tb_number')
		   ->select(DB::raw('number,COUNT(id) as row'))
		   ->where('period',Session::get('pid'))
		   ->where('length','3')
		   ->groupBy('number')
		     ->skip(0)->take(1)
		   ->orderByRaw(DB::raw('COUNT(id) desc'))->first();
		   $max_buy3 = $max_buy3_data->number;
		   $sum_max_buy3 = Number::where('period',Session::get('pid'))
		   ->where('length','3')->where('number',$max_buy3_data->number)->sum('price');
		   		}
			}else {
			
		  $total2 = DB::table('tb_number')
		  	->where('length','2')
		  	->where('period',Session::get('pid'))
		  	->where('users',Auth::user()->id)
		  	->sum('price');
		  $total3 = DB::table('tb_number')
		  	->where('length','3')
		  	->where('period',Session::get('pid'))
		  	->where('users',Auth::user()->id)
		  	->sum('price');
		  		$total = DB::table('tb_number')
		  	 	->where('period',Session::get('pid'))
		  	 	->where('users',Auth::user()->id)
		    	->sum('price');
		    	$user = '';
		    	 $count_max_buy2 = Number::where(array('length'=>'2','period'=>Session::get('pid'),'users'=>Auth::user()->id))->count();
		   if($count_max_buy2 <=0)
		   {
		   	$max_buy2 ='0';
		   	$sum_max_buy2 = '0';
		   }else{
		    	$max_buy2_data = DB::table('tb_number')
			   ->select(DB::raw('number,COUNT(id) as row'))
			   ->where('period',Session::get('pid'))
			   ->where('length','2')
			   ->where('users',Auth::user()->id)
			   ->groupBy('number')
			     ->skip(0)->take(1)
			   ->orderByRaw(DB::raw('COUNT(id) desc'))
			 ->first();
			 $max_buy2 = $max_buy2_data->number;
			 $sum_max_buy2 = Number::where('period',Session::get('pid'))
		   ->where('length','2')->where('users',Auth::user()->id)->where('number',$max_buy2_data->number)->sum('price');
			}
			 $count_max_buy3 = Number::where(array('length'=>'3','period'=>Session::get('pid'),'users'=>Auth::user()->id))->count();
		   if($count_max_buy3 <=0)
		   {
		   	$max_buy3 ='0';
		   	$sum_max_buy3 = '0';
		   }else{
			 $max_buy3_data = DB::table('tb_number')
			   ->select(DB::raw('number,COUNT(id) as row'))
			   ->where('period',Session::get('pid'))
			   ->where('length','3')
			   ->where('users',Auth::user()->id)
			   ->groupBy('number')
			     ->skip(0)->take(1)
			   ->orderByRaw(DB::raw('COUNT(id) desc'))
			 ->first();
			 $max_buy3 = $max_buy3_data->number;
			 $sum_max_buy3 = Number::where('period',Session::get('pid'))
		   ->where('length','3')->where('users',Auth::user()->id)->where('number',$max_buy3_data->number)->sum('price');
			}

			}
			$count_check = Period::where('pdate',date('Y-m-d'))->count();
     			if($count_check > 0){
			$period = Period::find(Session::has('pid'));
			$count_price = Number::where(array('period'=>Session::has('pid')))->sum('price');
			 $max_allow = number_format($period->price - $count_price);
			}else{
				$max_allow = '0';
			}
			return View::make('dashboard')->with(array(
					'total'=>number_format($total),
	     			 	'total2'=>number_format($total2),
	     			 	'total3'=>number_format($total3),
	     			 	'user' =>$user,
	     			 	'max_buy2' =>$max_buy2,
	     			 	'max_buy3' =>$max_buy3,
	     			 	'sum_max_buy2' =>number_format($sum_max_buy2),
	     			 	'sum_max_buy3' => number_format($sum_max_buy3),
	     			 	'max_allow'		=>$max_allow

     			 	));
		}else{
			return   Redirect::to('/');
		}
	}
	public function getOpen()
	{
		if(Auth::check()){
			if(Session::has('pid')){
			$period = Period::find(Session::get('pid'));
			$condition = ['period'=>Session::get('pid')];
			$enumber = Enumber::where($condition)->get();
			 
			return View::make('haveopen')->with('p',$period)->with('enumber',$enumber);
			}else{
			return View::make('open');
		 }
		 
		}else{
			return   Redirect::to('/');
		}
	}
	 
	 
	public function postOpen()
	{

		$d = explode('-',Input::get('dateinput'));
		$date = $d[2].'-'.$d[1].'-'.$d[0];
		 
		//$date = Input::get('dateinput');
		$count = Period::where('pdate',$date)->count();
		 
		if($count <= 0 )
		{
		 DB::table('tb_period')->insert(array(
				'pdate'		=>$date,
				'price'	=>Input::get('price_period')

			));
		}else{
		DB::table('tb_period')->where('pdate',$date)->update(array(
			'price' =>Input::get('price_period')
			));
		}
	/*	$gid=DB::table('tb_period')
		->select('id')
		->where('pdate',$date)
		->where('period',Input::get('period'))
		->get();
		foreach ($gid as $key =>$id){
			return $gid->id;
		}*/
		$condition = ['pdate'=> $date];
		$p = Period::where($condition)->get();
		foreach ($p as $k => $id){
			
			Session::put('pid', $id->id);
		}
		$price = Input::get('price');
		$number = Input::get('number');
		 
		foreach( $number as $index => $n )  {
			if($n != null && $price[$index] != null)
			{


				$count = Enumber::where(array('number'=>$n,'period'=>Session::get('pid')))->count();
				if($count<=0) {
				 DB::table('tb_enumber')
				 			->insert(array(
				 				'number'	=>$n,
				 				'price'		=>$price[$index],
				 				'period'	=>Session::get('pid')	

				 				));
				 		}else{
				 		DB::table('tb_enumber')
				 		->where(array('period'=>Session::get('pid'),'number'=>$n))
				 		->update(array('price'=>$price[$index]));
				 		}
				

			}
		}

		return 'ok';
		
		
	}
	public function getSold()
	{
		if(Auth::check()){
			
			if (Session::has('pid')) {
			if (!Session::has('list')){
				Session::put('list', '1');
			}
			$period = Period::find(Session::has('pid'));
			$count_price = Number::where(array('period'=>Session::has('pid')))->sum('price');
			 $max_allow = number_format($period->price - $count_price);
			 $sold = number_format($period->price);
			if($count_price >= $period->price)
			{
			return View::make('err_maxallow');
			}else{
			return View::make('sold')
			->with(array('p'=>$period,'total'=>number_format($count_price),'max_allow'=>$max_allow,'sold'=>$sold ));
			}
		}else{
			return View::make('open');
		}
		}else{
			return   Redirect::to('/');
		}
	}
	public function postSold()
	{
		 $price = Input::get('price');
		 $number = Input::get('number');

		 
		  $period = Period::find(Session::get('pid'));
		  $max = $period->price;
		  $count_price = Number::where(array('period'=>Session::get('pid')))->sum('price');
		  $N = new Number;
		
		 $sum = array_sum($price);
		 $total = $count_price + $sum;
		 $minute = $max - $total;
		  if($minute <0)
		 {
		 	return 'no';
		   }else{
		   	 
		   	foreach($number as $index=>$n){

			if($n !=null && $price[$index] !=null){
				$ln = Str::length($n);
			 	DB::table('tb_number')->insert(array(
			 	'number' => $n,
			 	'price'	 =>	$price[$index],
				'period' => Session::get('pid'),
				'list'	 => Input::get('list_id'),
				'length' => $ln,	
				'created_at' =>date('Y-m-d H:i:s'),
				'users'		=>Auth::user()->id

			 	));
			}	 
			}
		 
			return 'ok';
		  }
		
		 

	 
		  
		// return $sum;
	}
	public function postNewlist()
	{
		Session::forget('list');
		$oldlist = Input::get('lid');
		$newlist = $oldlist+1;
		Session::put('list',$newlist);
		 return 'ok';

	 
	}
	public function getR2length()
	{
		  if(Session::has('pid')){
		  	if(Auth::user()->usertype == 1)
		  	{
		  $Number = DB::table('tb_number as n')
		  ->select('n.number','n.price',DB::raw('SUM(n.price) as sumprice ')
		  	,'tb_period.pdate')
		  ->join('tb_period','tb_period.id','=','n.period')
		 //  ->join('tb_enumber','tb_enumber.period','=','n.period')
		  ->where('n.length','2')
		  	->where('n.period',Session::get('pid'))
		  ->orderBy('n.number','asc')
		  ->groupBy('n.number')
		  ->get();
		  $p = Period::find(Session::get('pid'));
		  $condition = ['period'=>Session::get('pid')];
		  $e= Enumber::where($condition)->get();
		  $total = DB::table('tb_number')->where('period',Session::get('pid'))->where('length','2')->sum('price');
			}else {
			$Number = DB::table('tb_number as n')
		  ->select('n.number','n.price',DB::raw('SUM(n.price) as sumprice ')
		  	,'tb_period.pdate')
		  ->join('tb_period','tb_period.id','=','n.period')
		 //  ->join('tb_enumber','tb_enumber.period','=','n.period')
		  	->where('n.period',Session::get('pid'))
		  ->where('n.length','2')
		  	->where('n.users',Auth::user()->id)
		  	
		  ->orderBy('n.number','asc')
		  ->groupBy('n.number')
		  ->get();
		  $p = Period::find(Session::get('pid'));
		  $condition = ['period'=>Session::get('pid')];
		  $e= Enumber::where($condition)->get();
		  $total = DB::table('tb_number')->where('period',Session::get('pid'))
		  	->where('length','2')->where('users',Auth::user()->id)->sum('price');
			}
		 // ,'tb_enumber.number as enumber','tb_enumber.price as eprice'

		 return View::make('r2length')
		 ->with('Number',$Number)
		 ->with('p',$p)
		 ->with('enumber',$e) 
		 ->with('total',$total);
		}else{
			return View::make('open');

		}
	}
	public function getR3length()
	{
		  
		  if(Session::has('pid')){
		  	if(Auth::user()->usertype == 1)
		  	{
		  $Number = DB::table('tb_number as n')
		  ->select('n.number','n.price',DB::raw('SUM(n.price) as sumprice ')
		  	,'tb_period.pdate')
		  ->join('tb_period','tb_period.id','=','n.period')
		 //  ->join('tb_enumber','tb_enumber.period','=','n.period')
		  ->where('n.length','3')
		  	->where('n.period',Session::get('pid'))
		  ->orderBy('n.number','asc')
		  ->groupBy('n.number')
		  ->get();
		  $p = Period::find(Session::get('pid'));
		  $condition = ['period'=>Session::get('pid')];
		  $e= Enumber::where($condition)->get();
		  $total = DB::table('tb_number')->where('period',Session::get('pid'))->where('length','3')->sum('price');
		 // ,'tb_enumber.number as enumber','tb_enumber.price as eprice'
		}else{
		$Number = DB::table('tb_number as n')
		  ->select('n.number','n.price',DB::raw('SUM(n.price) as sumprice ')
		  	,'tb_period.pdate')
		  ->join('tb_period','tb_period.id','=','n.period')
		 //  ->join('tb_enumber','tb_enumber.period','=','n.period')
		  ->where('n.length','3')
		  	->where('n.period',Session::get('pid'))
		  	->where('n.users',Auth::user()->id)
		  ->orderBy('n.number','asc')
		  ->groupBy('n.number')
		  ->get();
		  $p = Period::find(Session::get('pid'));
		  $condition = ['period'=>Session::get('pid')];
		  $e= Enumber::where($condition)->get();
		  $total = DB::table('tb_number')->where('period',Session::get('pid'))->where('length','3')->where('users',Auth::user()->id)->sum('price');
		 // ,'tb_enumber.number as enumber','tb_enumber.price as eprice'

		}

		 return View::make('r3length')
		 ->with('Number',$Number)
		 ->with('p',$p)
		 ->with('enumber',$e) 
		 ->with('total',$total);
		}else{
			return View::make('open');

		}
	}
	public function getCheck()
	{
		if(Auth::check()){
			if(Session::has('pid')){
			$period = Period::find(Session::get('pid'));
			$condition = ['period'=>Session::get('pid')];
			$enumber = Enumber::where($condition)->get();
			return View::make('check')->with('p',$period)->with('enumber',$enumber);
			}else{
			return View::make('open');
			}
		}else{
			return   Redirect::to('/');
		}
	}
	public function postCheck(){
		if(Auth::check()){
			if(Session::has('pid')){
		 	 $condition =['r2length'=>Input::get('n2length'),'r3length'=>Input::get('n3length'),'period'=>Session::get('pid')];
			$count = Result::where($condition)->count();


			if($count <=0){
			DB::table('tb_result')->insert(array(
				'r2length'	=> Input::get('n2length'),
				'r3length'	=> Input::get('n3length'),
				'period'	=> Session::get('pid')

				));
			$condition2 =['number'=>Input::get('n2length'),'period'=>Session::get('pid'),'length'=>'2'];
		 	 $condition3 =['number'=>Input::get('n3length'),'period'=>Session::get('pid'),'length'=>'3'];
			$count2 = Number::where($condition2)->count();
			$count3 = Number::where($condition3)->count();
			$total2 = DB::table('tb_number')
					->where('length','2')
					->where('number',Input::get('n2length'))
					->where('period',Session::get('pid'))
					->sum('price');
			$total3 = DB::table('tb_number')
					->where('length','3')
					->where('number',Input::get('n3length'))
					->where('period',Session::get('pid'))
					->sum('price');

			Session::put('r2',Input::get('n2length'));
			Session::put('r3',Input::get('n3length'));
			Session::put('c2',$count2);
			Session::put('c3',$count3);
			Session::put('t2',$total2);
			Session::put('t3',$total3);
			}else{
		 	 $condition2 =['number'=>Input::get('n2length'),'period'=>Session::get('pid'),'length'=>'2'];
		 	 $condition3 =['number'=>Input::get('n3length'),'period'=>Session::get('pid'),'length'=>'3'];

				$count2 = Number::where($condition2)->count();
				$count3 = Number::where($condition3)->count();
				//return dd($count3);
				$total2 = DB::table('tb_number')
					->where('length','2')
					->where('number',Input::get('n2length'))
					->where('period',Session::get('pid'))
					->sum('price');
				$total3 = DB::table('tb_number')
					->where('length','3')
					->where('number',Input::get('n3length'))
					->where('period',Session::get('pid'))
					->sum('price');
				Session::put('r2',Input::get('n2length'));
				Session::put('r3',Input::get('n3length'));
				Session::put('c2',$count2);
				Session::put('c3',$count3);
				Session::put('t2',$total2);
				Session::put('t3',$total3);

				return View::make('result');
			}
			

			return 'ok';
			}else{
			return View::make('open');
			}
			}else{
			return   Redirect::to('/');
		}
	}
	public function getReport($type)
	{
		if(Auth::check())
		{
			if(Session::has('pid'))
			{
				switch($type)
				{
					case '2length':
						return View::make('report_2length');
					break;
					case '3length':

					break;
				}
			}else{
			return View::make('open');
			}


		}else{
			return Redirect::to('/');
		}
	}
	public function postBegincheck(){
		if(Auth::check()){
			if(Session::has('pid')){
			return View::make('result');
			}else{
			return View::make('open');
			}
			}else{
			return   Redirect::to('/');
		}
	}

	public function getUser()
	{
		if(Auth::check()){
			$api = 'datauser';
			return View::make('user')->with(array('api'=>$api)); 
			 
		}else{
			return Redirect::to('/');
		}
	}
	public function getAdduser()
	{
		if(Auth::check()){
			$api = 'home/datauser';
			return View::make('adduser')->with(array('api'=>$api)); 
			 
		}else{
			return Redirect::to('/');
		}
	}
	public function postAdduser()
	{
		if(Auth::check()){
			 $rules = array(
			'txt-name'	=> 'required',
			'txt-username'	=> 'required',
			'txt-mobile'	=> 'required',
			'txt-address'	=> 'required',
			'txt-password2'	=> 'required|min:6',
			'txt-password'	=> 'required|min:6|same:txt-password',
			'txt-status'	=> 'required',
			'txt-type'	=> 'required'
			);
			 $msg = array(
			 		'txt-name.required' =>Lang::get('msg.User-Name',array(),'th').' '.Lang::get('msg.Required',array(),'th'),
			 		'txt-username.required' =>Lang::get('msg.User-Username',array(),'th').' '.Lang::get('msg.Required',array(),'th'),
			 		'txt-mobile.required' =>Lang::get('msg.User-Mobile',array(),'th').' '.Lang::get('msg.Required',array(),'th'),
			 		'txt-address.required' =>Lang::get('msg.User-Address',array(),'th').' '.Lang::get('msg.Required',array(),'th'),
			 		'txt-password.required' =>Lang::get('msg.User-Password',array(),'th').' '.Lang::get('msg.Required',array(),'th'),
			 		'txt-password2.required' =>Lang::get('msg.User-ConfirmPassword',array(),'th').' '.Lang::get('msg.Required',array(),'th'),
			 		'txt-status.required' =>Lang::get('msg.User-Status',array(),'th').' '.Lang::get('msg.Required',array(),'th'),
			 		'txt-type.required' =>Lang::get('msg.User-Type',array(),'th').' '.Lang::get('msg.Required',array(),'th')

			 	);
				$validator = Validator::make(Input::all(), $rules,$msg);

    		if ($validator->fails())
    			{
    				return Redirect::to('home/adduser')->withErrors($validator)->withInput();
    			}else{
    				$u = new User;
    				$u->userid = Input::get('txt-username');
    				$u->password = Hash::make(Input::get('txt-password'));
    				$u->name = Input::get('txt-name');
    				$u->mobile = Input::get('txt-mobile');
    				$u->address = Input::get('txt-address');
    				$u->usertype = Input::get('txt-type');
    				$u->userstatus = Input::get('txt-status');
    				$u->updated_at = date('Y-m-d H:i:s');
    				$u->created_at = date('Y-m-d H:i:s');
    				$u->save();
    				return Redirect::to('/home/user');

    			}
			 
		}else{
			return Redirect::to('/');
		}
	}
	public function getDatauser()
	{
		$sql = User::select([
			 'id','name','userid','mobile','address','usertype','userstatus'])
		->orderBy('id','desc');
		$datatables = Datatables::of($sql)
		->removeColumn('id')
		->addColumn('no','')
		->editColumn('usertype','@if($usertype==1)  <a href=\'{{ URL::to(\'home/unpublic/usertype\',array($id))}}\'><i class="fa fa-user fa-2x"></i></a>@else  <a href=\'{{ URL::to(\'home/public/usertype\',array($id))}}\'><i class="fa fa-child fa-2x"></i></i></a> @endif')
		->editColumn('userstatus','@if($userstatus==1)  <a href=\'{{ URL::to(\'home/unpublic/userstatus\',array($id))}}\'><i class="fa fa-check-circle fa-2x"></i></a>@else  <a href=\'{{ URL::to(\'home/public/userstatus\',array($id))}}\'><i class="fa fa-ban fa-2x"></i></i></a> @endif')
	       	->addColumn('tools','
			<a href="{{ URL::to(\'home/edituser\',array($id))}}" class="btn btn-info"  >
			<i class="fa fa-pencil"></i>
			 {{ Lang::get(\'msg.Edit\',array(),\'th\')}}
			</a>
			<a href="{{ URL::to(\'home/del/user\',array($id)) }}" class="btn btn-danger" onclick="javascript:return confirm(\'{{ Lang::get(\'msg.Confirm_del\',array(),\'th\') }}\')" >
			<i class="fa fa-trash"></i>
			 {{ Lang::get(\'msg.Delete\',array(),\'th\')}}
			</a>
			
			');
		 

        return $datatables->make(true);
	}
	public function getPublic($t,$id) {
		if(Auth::check()){
		switch ($t) {
			case 'usertype':
				User::where('id',$id)->update(array('usertype'=>'1'));
				break;
			
			case 'userstatus':
				User::where('id',$id)->update(array('userstatus'=>'1'));
				break;
		}
		return Redirect::to('/home/user');
	}else{
		return Redirect::to('/');
	}
	}
	public function getUnpublic($t,$id) {
		if(Auth::check()){
		switch ($t) {
			case 'usertype':
				User::where('id',$id)->update(array('usertype'=>'2'));
				break;
			
			case 'userstatus':
				User::where('id',$id)->update(array('userstatus'=>'2'));
				break;
		}
		return Redirect::to('/home/user');
	}else{
		return Redirect::to('/');
	}
	}

	  public function getDel($t,$id)
	{
		if(Auth::check()) {
		switch ($t) {
			case 'user':
				User::find($id)->delete();
				break;
			
		 
		}
		return Redirect::to('/home/user');
	}else{
		return Redirect::to('/');
	}
	}
	public function getEdituser($id)
	{
		if(Auth::check()){
			$u = User::find($id);
			return View::make('edituser')->with(array('u'=>$u));
		}else{
			return Redirect::to('/home/user');
		}
		
	}
	public function postEdituser()
	{
		if(Auth::check())
		{
			$id = Input::get('id');
			$u = User::find($id);
			$password = Input::get('txt-password');
		 	$newpassword = Hash::make($password);
			  if($password =='' || $password ==null)
			  {
			  	$u->userid = Input::get('txt-username');
			  	$u->name = Input::get('txt-name');
    				$u->mobile = Input::get('txt-mobile');
    				$u->address = Input::get('txt-address');
    				$u->usertype = Input::get('txt-type');
    				$u->userstatus = Input::get('txt-status');
    				$u->updated_at = date('Y-m-d H:i:s');
    				 
    				$u->save();
			  }else{
			  	$u->userid = Input::get('txt-username');
			  	$u->name = Input::get('txt-name');
			  	$u->password = $newpassword;
    				$u->mobile = Input::get('txt-mobile');
    				$u->address = Input::get('txt-address');
    				$u->usertype = Input::get('txt-type');
    				$u->userstatus = Input::get('txt-status');
    				$u->updated_at = date('Y-m-d H:i:s');
    			 
    				$u->save();
			  }
			  return Redirect::to('home/user');
		}else{
			return Redirect::to('/');
		}
	}

	public function getChecksold($n,$p)
	{
		if(Auth::check()) 
		{
			$pid = Session::has('pid');
			//if(Session::has('pid')){
			$period = Period::find(Session::get('pid'));
		//	return $period->pdate;
			$count_number = Enumber::where(array('number'=>$n,'period'=>$pid))->count();
			if($count_number <= 0)
			{
				return '<span class="text-success"><i class="fa fa-check-circle"> '.Lang::get('msg.Number',array(),'th').' '.$n.' '.Lang::get('msg.CanSold',array(),'th').'</span>';
			}else{
				$check_price = Enumber::where(array('number'=>$n,'period'=>$pid))->first();
				$count_price = Number::where(array('number'=>$n,'period'=>$pid))->sum('price');
				 
			 	$sum_price = $p+$count_price;
				if($sum_price <= $check_price->price ) 
				{
					$min_price =   $check_price->price - $sum_price;
					return '<span class="text-success"><i class="fa fa-check-circle"></i> '.Lang::get('msg.Number',array(),'th').' '.$n.' '.Lang::get('msg.CanSold2',array(),'th').'</span><span class="text-danger"> '.$min_price.' '.Lang::get('msg.Currency',array(),'th').'</span>';
				}else{
					$min_price =   $check_price->price - $count_price;
					return '<span class="text-danger"><i class="fa fa-ban"></i> '.Lang::get('msg.Number',array(),'th').' '.$n.' '.Lang::get('msg.CantSold',array(),'th').' '.$check_price->price.' '.Lang::get('msg.Currency',array(),'th').'</span><span class="text-success"> '.Lang::get('msg.Number',array(),'th').' '.$n.' '.Lang::get('msg.CanSold2',array(),'th').'</span><span class="text-danger"> '.$min_price.' '.Lang::get('msg.Currency',array(),'th').'</span>';
				}
			}
		}else{
			return Redirect::to('/');
		}
	}


}
