@extends('master')

@section('content')
<section class="content-header">
<h4 >
                        
                    </h4>
 
</section>
<section class="content">

		<div class="row">


                        <div class="col-md-12">
                            <!-- Primary box -->
                            <div class="box box-primary">
                                <div class="box-header" >
                                    <h3 class="box-title"></h3>
                                    
                                </div>
                                <div class="box-body">
                                    

                                                 
                                                 <div class="box box-solid box-primary">
                                <div class="box-header">
                                    <h3 class="box-title"><i class="fa fa-star fa-lg"></i>
                                    {{ Lang::get('msg.Check', array(), 'th')}}
                                    </h3>
                                    
                                </div>
                                <div class="box-body">
                                    <form action="#" id="form_check" method="POST">
                                        
                                  <div class="row" style="padding-bottom:5px;">
                                     
                                     <div class="col-xs-4">
                                         <div class="input-group">
                                            <span class="input-group-addon" id="basic-addon1">
                                            <i class="fa fa-calendar"></i>

                                                {{ Lang::get('msg.Period',array(), 'th') }}</span>
                                            
                                        <input type="text" id="dateInput" value="{{ Helpers::ConvDate($p->pdate) }}" name="dateinput" class="form-control" placeholder="" aria-describedby="basic-addon1"  >
                                        </div>

                                    </div>
                                    
                                    
                                 </div>
                                                                       
                                 <div class="row" style="padding-bottom:5px;">

                                    <div class="col-xs-4">
                                        <div class="input-group">
                                            <span class="input-group-addon" id="basic-addon1">
                                                <i class="fa fa-check-circle"></i> {{ Lang::get('msg.2lenght', array(), 'th')}}  </span>
                                        <input type="text"  name="n2length" class="form-control" placeholder="" value="{{ Session::get('r2') }}" aria-describedby="basic-addon1">
                                        </div>
                                     </div>
                                     <div class="col-xs-4">
                                         <div class="input-group">

                                            <span class="input-group-addon" id="basic-addon1">
											<i class="fa fa-check-circle"></i> 
                                            	{{ Lang::get('msg.3lenght', array(), 'th')}}</span>
                                        <input type="text" value="{{ Session::get('r3') }}" name="n3length" class="form-control" placeholder="" aria-describedby="basic-addon1">
                                        </div>

                                    </div>
                                     
                                    
                                 </div>
                                
                                        <div style="margin-top:10px"></div>

                                    
                                       
                                 <button type="button" id="create-check" class="btn btn-primary btn-block btn-lg"><i class="fa fa-search fa-lg"></i> {{ Lang::get('msg.Ok',array(), 'th') }}</button>
                                        
                                    </form>
                                </div>
                            
                                              </div>
                                </div><!-- /.box-body -->
                                <div class="box-footer">
                                    
                                </div><!-- /.box-footer-->

                                
                            </div><!-- /.box -->
                            <div style="margein-top:10px;" id="show-check"></div>

                        </div><!-- /.col -->

                        
</section>  
@stop