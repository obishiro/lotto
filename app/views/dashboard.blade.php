@extends('master')

@section('content')
<section class="content-header">
<h1>
<i class="fa fa-home fa-2x"></i>
 
{{ Lang::get('msg.WebName',array(),'th') }}
</h1>
 
</section>
<section class="content">
   <!-- Small boxes (Stat box) -->
                    <div class="row">
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-aqua">
                                <div class="inner">
                                    <h3>
                                        {{ $total2 }}<sup style="font-size: 20px">{{ Lang::get('msg.Currency',array(),'th')}}</sup>
                                    </h3>
                                    <p>
                                        {{ Lang::get('msg.2lenght',array(),'th')}}
                                    </p>
                                </div>
                                <div class="icon">
                                  2
                                </div>
                                <a href="#" class="small-box-footer">
                                     <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div><!-- ./col -->
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-green">
                                <div class="inner">
                                    <h3>
                                        {{ $total3 }}<sup style="font-size: 20px">{{ Lang::get('msg.Currency',array(),'th')}}</sup>
                                    </h3>
                                    <p>
                                        {{ Lang::get('msg.3lenght',array(),'th')}}
                                    </p>
                                </div>
                                <div class="icon">
                                    3
                                </div>
                                <a href="#" class="small-box-footer">
                                     <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div><!-- ./col -->
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-yellow">
                                <div class="inner">
                                    <h3>
                                        {{ $total }}<sup style="font-size: 20px">{{ Lang::get('msg.Currency',array(),'th')}}</sup>
                                    </h3>
                                    <p>
                                        {{ Lang::get('msg.Total',array(),'th')}}
                                    </p>
                                </div>
                                <div class="icon">
                                  <i class="fa fa-money"></i>
                                </div>
                                <a href="#" class="small-box-footer">
                                     <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div><!-- ./col -->
                       
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box  bg-red ">
                                <div class="inner">
                                    <h3>
                                       {{ $max_allow }}<sup style="font-size: 20px">{{ Lang::get('msg.Currency',array(),'th')}}</sup>
                                    </h3>
                                    <p>
                                       {{ Lang::get('msg.CanSold2', array(), 'th') }}
                                    </p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-money"></i>
                                </div>
                                <a href="#" class="small-box-footer">
                                     <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div><!-- ./col -->
                       
                    </div><!-- /.row -->
                  <div class="row">
                  	   <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-red">
                                <div class="inner">
                                    <h3>
                                        {{ $max_buy2 }} 
                                    </h3>
                                    <p>
                                        {{ Lang::get('msg.Total',array(),'th')}}
                                        {{ $sum_max_buy2 }} {{ Lang::get('msg.Currency',array(),'th')}}
                                    </p>
                                </div>
                                <div class="icon">
                                  2
                                </div>
                                <a href="#" class="small-box-footer">
                                     <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div><!-- ./col -->
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-yellow">
                                <div class="inner">
                                    <h3>
                                       {{ $max_buy3 }} 
                                    </h3>
                                    <p>
                                        {{ Lang::get('msg.Total',array(),'th')}}
                                        {{ $sum_max_buy3 }} {{ Lang::get('msg.Currency',array(),'th')}}

                                    </p>
                                </div>
                                <div class="icon">
                                    3
                                </div>
                                <a href="#" class="small-box-footer">
                                     <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div><!-- ./col -->
                        @if(Auth::user()->usertype == 1)
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-green">
                                <div class="inner">
                                    <h3>
                                        {{ $user }}
                                    </h3>
                                    <p>
                                        {{ Lang::get('msg.User',array(), 'th') }}
                                    </p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-user"></i>
                                </div>
                                <a href="#" class="small-box-footer">
                                     <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div><!-- ./col -->
                        @endif
                         <!-- ./col -->
                  </div>


</section>  
@stop