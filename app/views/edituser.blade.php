@extends('master')

@section('content')
<section class="content-header">
<h4 >
                        
                    </h4>
 
</section>
<section class="content">

		<div class="row">


                        <div class="col-md-12">

								

                            <!-- Primary box -->

                            <div class="box box-primary">
                                <div class="box-header" >
                                    
                                   
                                </div>
                                <div class="box-body">
                                   
                                                 
                                 <div class="box box-solid box-info">
                                <div class="box-header">
                                    <h3 class="box-title"><i class="fa fa-pencil"></i> {{ Lang::get('msg.EditUser',array(),'th')}}
                                    </h3>
                                    
                                </div>
                                <div class="box-body">
                               
                                {{ Form::open(array(
                                	'url' => 'home/edituser',
                                	'method' => 'post'

                                	))}}
								  <div class="form-group">
								    <label for="exampleInputEmail1" >{{ Lang::get('msg.User-Name',array(),'th')}}</label>
								    <input type="text" class="form-control" id="" name="txt-name" placeholder="{{ Lang::get('msg.User-Name',array(),'th')}}"  value="{{ $u->name}}">
									 @if ($errors->has('txt-name')) <p class="text-danger ">{{ $errors->first('txt-name') }}</p> @endif
								  </div>
								  <div class="form-group">
								    <label for="exampleInputPassword1">{{ Lang::get('msg.User-Username',array(),'th')}}</label>
								    <input type="text" class="form-control" id="" name="txt-username" placeholder="{{ Lang::get('msg.User-Username',array(),'th')}}"   value="{{ $u->userid}}">
								    @if ($errors->has('txt-username')) <p class="text-danger ">{{ $errors->first('txt-username') }}</p> @endif
								  </div>
								  <div class="form-group">
								    <label for="exampleInputPassword1">{{ Lang::get('msg.User-Password',array(),'th')}}</label>
								    <input type="password" class="form-control" id="" name="txt-password" placeholder="{{ Lang::get('msg.User-Password',array(),'th')}}" {{ (Input::old('txt-password')) ? 'value= "'.Input::old('txt-password').'"' : '' }}>
								    @if ($errors->has('txt-password')) <p class="text-danger ">{{ $errors->first('txt-password') }}</p> @endif
								  </div>
								   
								  <div class="form-group">
								    <label for="exampleInputPassword1">{{ Lang::get('msg.User-Mobile',array(),'th')}}</label>
								    <input type="text" class="form-control" id="" name="txt-mobile" placeholder="{{ Lang::get('msg.User-Mobile',array(),'th')}}"   value="{{ $u->mobile}}">
								    @if ($errors->has('txt-mobile')) <p class="text-danger ">{{ $errors->first('txt-mobile') }}</p> @endif
								  </div>
								  <div class="form-group">
								    <label for="exampleInputPassword1">{{ Lang::get('msg.User-Address',array(),'th')}}</label>
								    <input type="text" class="form-control" id="" name="txt-address" placeholder="{{ Lang::get('msg.User-Address',array(),'th')}}"   value="{{ $u->address}}">
								    @if ($errors->has('txt-address')) <p class="text-danger ">{{ $errors->first('txt-address') }}</p> @endif
								  </div>
								   <div class="form-group">
								    <label for="exampleInputPassword1">{{ Lang::get('msg.User-Type',array(),'th')}}</label>
								   	<select name="txt-type" id="" class="form-control">
								   		<option value="">{{ Lang::get('msg.Choose',array(),'th')}}</option>
								   		<option value="1" @if ($u->usertype=="1") selected='true'   @endif>{{ Lang::get('msg.User-Admin',array(),'th')}}</option>
								   		<option value="2" @if ($u->usertype=="2") selected='true' @endif>{{ Lang::get('msg.User-Public',array(),'th')}}</option>
								   	</select>
								   	@if ($errors->has('txt-type')) <p class="text-danger ">{{ $errors->first('txt-type') }}</p> @endif
								  </div>
								  <div class="form-group">
								    <label for="exampleInputPassword1">{{ Lang::get('msg.User-Status',array(),'th')}}</label>
								   	<select name="txt-status" id="" class="form-control">
								   		<option value="">{{ Lang::get('msg.Choose',array(),'th')}}</option>
								   		<option value="1" @if ($u->userstatus=="1") selected='true' @endif>{{ Lang::get('msg.User-Approve',array(),'th')}}</option>
								   		<option value="2" @if ($u->userstatus=="2") selected='true' @endif>{{ Lang::get('msg.User-Ban',array(),'th')}}</option>
								   	</select>
								   	@if ($errors->has('txt-status')) <p class="text-danger ">{{ $errors->first('txt-status') }}</p> @endif
								  </div>
								  
								  <button type="submit" class="btn btn-primary col-md-9">{{ Lang::get('msg.Ok',array(),'th')}}</button>
								  <button type="reset" class="btn btn-danger col-md-2" style="margin-left:10px">{{ Lang::get('msg.Cancle',array(),'th')}}</button>
								  {{ Form::hidden('id', $u->id)}}
								</form>


                                </div>
                            
                                </div>
                                
                                </div><!-- /.box-body -->
                                <div class="box-footer">
                                    
                                </div><!-- /.box-footer-->
                            </div><!-- /.box -->
                        </div><!-- /.col -->

                        
</section>  
@stop