@extends('master')

@section('content')
<section class="content-header">
<h4 >
                        
                    </h4>
 
</section>
<section class="content">

		<div class="row">


                        <div class="col-md-12">
                            <!-- Primary box -->
                            <div class="box box-primary">
                                <div class="box-header" >
                                    <h3 class="box-title"></h3>
                                    
                                </div>
                                <div class="box-body">
                                    

                                                 
                                 <div class="box box-solid box-danger">
                                <div class="box-header">
                                    <h3 class="box-title"><i class="fa fa-lock fa-lg"></i>
                                    {{ Lang::get('msg.Close', array(), 'th')}}
                                    </h3>
                                    
                                </div>
                                <div class="box-body">
                                     <div class="callout callout-danger" style ="text-align:center">
                                        <h1 class="text-danger">
                                        <i class="fa fa-exclamation-triangle fa-5x" aria-hidden="true"></i> 
                                        </h1>
                                        <h1 class="text-danger">
                                        {{ Lang::get('msg.text_close_maxallow',array(),'th')}}
                                        </h1>
                                     </div>
                                </div>
                            
                                 </div>
                                
                                </div><!-- /.box-body -->
                                <div class="box-footer">
                                    
                                </div><!-- /.box-footer-->
                            </div><!-- /.box -->
                        </div><!-- /.col -->

                        
</section>  
@stop