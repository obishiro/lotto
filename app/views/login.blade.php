<!DOCTYPE html>
<html class="bg-black">
 
    <head>
        <meta charset="UTF-8">
        <title>{{ Lang::get('msg.WebName',array(),'th') }}</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        {{ HTML::style('css/bootstrap.min.css')}}
        {{ HTML::style('css/font-awesome.min.css')}}
        {{ HTML::style('css/ionicons.min.css')}}
        {{ HTML::style('css/AdminLTE.css')}}
    </head>
    <body class="bg-black" >

        <div class="form-box"  id="login-box">
            <div class="header">{{ Lang::get('msg.Login', array(),'th') }}</div>
                {{ Form::open(array('url' => 'home/login')) }}
                <div class="body bg-gray">
              
                    <div class="form-group" @if ($errors->has('userid')) has-error @endif">
                        <input type="text" name="userid" class="form-control" placeholder="{{ Lang::get('msg.Username', array(),'th') }}" {{ (Input::old('userid')) ? 'value= "'.Input::old('userid').'"' : '' }}/>
                        @if ($errors->has('userid'))
                         <div class="callout callout-danger">
                            <i class="fa fa-warning"></i>
                            {{ $errors->first('userid',Lang::get('msg.Required', array(), 'th')) }}
                         </div>
                         @endif
                    </div>
                    <div class="form-group" @if ($errors->has('password')) has-error @endif">
                        <input type="password" name="password" class="form-control" placeholder="{{ Lang::get('msg.Password', array(),'th') }}"/>
                        @if ($errors->has('password'))
                         <div class="callout callout-danger">
                            <i class="fa fa-warning"></i>
                            {{ $errors->first('password',Lang::get('msg.Required', array(), 'th')) }}
                         </div>
                         @endif
                    </div>          
                  <!--  <div class="form-group">
                        <input type="checkbox" name="remember_me"/> Remember me
                    </div>-->
                </div>
                <div class="footer">                                                               
                    <button type="submit" id="bt-login" class="btn bg-olive btn-block">

                        {{ Lang::get('msg.Btlogin', array(),'th') }}</button>  
                    
                     
                    
                   
                </div>
            </form>

            
        </div>

        {{ HTML::script('js/jquery.min.js')}}
        {{ HTML::script('js/bootstrap.min.js')}}
        {{ HTML::script('js/jquery.countdown.min.js')}}
 
        {{ HTML::script('js/libs.js')}}
      
 
    </body>
</html>