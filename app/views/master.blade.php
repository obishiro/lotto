<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>{{ Lang::get('msg.WebName',array(),'th') }}</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        {{ HTML::style('css/bootstrap.min.css')}}
        {{ HTML::style('css/font-awesome.min.css')}}
        {{ HTML::style('css/ionicons.min.css')}}
        {{ HTML::style('css/AdminLTE.css')}}
        {{ HTML::style('css/datatables/dataTables.bootstrap.css')}}
        {{ HTML::style('css/datepicker/datepicker3.css')}}
         

 

         
      
        

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="skin-blue" onLoad="checkdate()">
        <!-- header logo: style can be found in header.less -->
        <header class="header">
            <a href="/" class="logo">
                <!-- Add the class icon to your logo image or logo icon to add the margining -->
                 {{ Lang::get('msg.WebName',array(), 'th')}}
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                
            </nav>
        </header>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            @include('menu')

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">  

            <div class="row">
                 <div id="clock" style="float:right;margin-right:2%;padding:10px"></div>  
            </div>
           
            @yield('content')
               
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->


        <!-- jQuery 2.0.2 -->
        {{ HTML::script('js/jquery.min.js')}}
        {{ HTML::script('js/bootstrap.min.js')}}
        {{ HTML::script('js/AdminLTE/app.js')}}
        {{ HTML::script('js/AdminLTE/demo.js')}}
        {{ HTML::script('js/plugins/datepicker/bootstrap-datepicker.js')}}
        
      
         
        {{ HTML::script('js/plugins/datatables/jquery.dataTables.js')}}
        {{ HTML::script('js/plugins/datatables/dataTables.bootstrap.js')}}
        {{ HTML::script('js/libs.js')}}
          {{ HTML::script('js/jquery.countdown.min.js')}}
          {{ HTML::script('js/push.min.js')}}
            
        <script type="text/javascript">
            $(function() {
                
             var oTable=   $('#table_number').dataTable({
                    "bPaginate": true,
                    "bLengthChange": true,
                    "bFilter": true,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false,
                    "iDisplayLength": 100,


                });

             oTable.fnSort( [ [1,'asc'] ] );
                
            });
           // Push.create('Hello World!', {
           //  body: 'This is some body content!',
           //  icon: {
           //      x16: 'images/icon-x16.png',
           //      x32: 'images/icon-x32.png'
           //  },
           //  timeout: 5000
           //  });
        </script>
      
        @yield('script')
    
    </body>
</html>