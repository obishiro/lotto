   <aside class="left-side sidebar-offcanvas">                
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    
                    <!-- search form -->
                 
                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                     <li class="active">
                            <a href="{{ URL::to('home/dashboard') }}">
                                <i class="fa fa-home fa-2x"></i><span>{{ Lang::get('msg.Home', array(), 'th') }}</span>
                            </a>
                        </li>
                        @if(Auth::user()->usertype == 1)
                        <li class="active">
                            <a href="{{ URL::to('home/open') }}">
                                <i class="fa fa-bell-o fa-2x"></i><span>{{ Lang::get('msg.Open', array(), 'th') }}</span>
                            </a>
                        </li>
                        @endif
                        @if(Auth::user()->usertype == 2)
                        <li>
                            <a href="{{ URL::to('home/sold') }}">
                                <i class="fa fa-pencil-square-o fa-2x"></i><span>{{ Lang::get('msg.Sold', array(), 'th') }}</span>
                            </a>
                        </li>
                        @endif

                        <li>
                            <a href="{{ URL::to('home/check') }}">
                                <i class="fa fa-star fa-2x"></i><span>{{ Lang::get('msg.Check', array(), 'th') }}</span>
                            </a>
                        </li>
                        
                        <li >
                            <a href="{{ URL::to('home/r2length') }}">
                                <i class="fa fa-book fa-2x"></i><span>{{ Lang::get('msg.2lenght', array(), 'th') }} </span>
                            </a>
                        </li>

                         <li >
                            <a href="{{ URL::to('home/r3length') }}">
                                <i class="fa fa-book fa-2x"></i><span>{{ Lang::get('msg.3lenght', array(), 'th') }}</span>
                            </a>
                        </li>
                       <!-- <li >
                           <a href="{{ url('home/logout') }}"  >
                                <i class="fa fa-print fa-2x"></i><span>{{ Lang::get('msg.Report',array(), 'th') }}</span>
                                
                            </a>
                            
                        </li>-->
                          @if(Auth::user()->usertype == 1)
                         <li >
                           <a href="{{ url('home/user') }}" >
                                <i class="fa fa-user fa-2x"></i><span>{{ Lang::get('msg.User',array(), 'th') }}</span>
                                
                            </a>
                            
                        </li>
                        @endif
                       
                         <li >
                           <a  href="{{ url('home/logout') }}"  onClick="return confirm('Are you sure ?')">
                                <i class="fa fa-power-off fa-2x"></i><span>{{ Lang::get('msg.Logout',array(), 'th') }}</span>
                                
                            </a>
                            
                        </li>
                        
                    </ul>
                </section>
                <!-- /.sidebar -->
                                    
            </aside>
      