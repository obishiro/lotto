@extends('master')

@section('content')
<section class="content-header">
<h4 >
                        
                    </h4>
 
</section>
<section class="content">

		<div class="row">


                        <div class="col-md-12">
                            <!-- Primary box -->
                            <div class="box box-primary">
                                <div class="box-header" >
                                    <h3 class="box-title"></h3>
                                    
                                </div>
                                <div class="box-body">
                                    

                                                 
                                                 <div class="box box-solid box-danger">
                                <div class="box-header">
                                    <h3 class="box-title"><i class="fa fa-pencil fa-lg"></i>
                                    {{ Lang::get('msg.Open', array(), 'th')}}
                                    </h3>
                                    
                                </div>
                                <div class="box-body">
                                    <form action="#" id="form_create" method="POST">
                                        
                                  <div class="row" style="padding-bottom:5px;">
                                     
                                     <div class="col-xs-4">
                                         <div class="input-group">
                                            <span class="input-group-addon" id="basic-addon1">
                                            <i class="fa fa-calendar"></i>

                                                {{ Lang::get('msg.Period',array(), 'th') }}</span>
                                            
                                        <input type="text" id="dateInput" name="dateinput" class="form-control" placeholder="" aria-describedby="basic-addon1" value="">
                                        </div>

                                    </div>
                                    
                                    
                                 </div>
                                    <div class="row" style="padding-bottom:5px;">
                                     
                                     <div class="col-xs-4">
                                         <div class="input-group">
                                            <span class="input-group-addon" id="basic-addon1">
                                                <i class="fa fa-check-circle"></i> ยอดรับประจำงวด</span>
                                        <input type="text" name="price_period" class="form-control" placeholder="" aria-describedby="basic-addon1" value="">
                                        </div>

                                    </div>
                                    
                                    
                                 </div>
                               
                                     <div class="row" style="padding-bottom:5px;">

                                    <div class="col-xs-4">
                                        <div class="input-group">
                                            <span class="input-group-addon" id="basic-addon1">
                                                <i class="fa fa-times-circle"></i> เลขที่กั้นยอดรับ  </span>
                                        <input type="text" name="number[]" class="form-control" placeholder="" aria-describedby="basic-addon1" value="">
                                        </div>
                                     </div>
                                     <div class="col-xs-4">
                                         <div class="input-group">

                                            <span class="input-group-addon" id="basic-addon1">จำนวนเงิน</span>
                                        <input type="text" name="price[]" class="form-control" placeholder="" aria-describedby="basic-addon1" value="">
                                        </div>

                                    </div>
                                    <div class="col-xs-2">
                                         <button type="button" id="add-number" class="btn btn-info"><i class="fa fa-lg fa-plus-circle"></i> เพิ่มตัวเลข</button>
                                     </div>
                                    
                                 </div>
                                <div id="append">
                                </div>
                                      
                                        <div style="margin-top:10px"></div>

                                    
                                       
                                 <button type="button" id="create-period" class="btn btn-success btn-block btn-lg"><i class="fa fa-check-circle fa-lg"></i> {{ Lang::get('msg.Ok',array(), 'th') }}</button>
                                        
                                    </form>
                                </div>
                            
                                              </div>
                                
                                </div><!-- /.box-body -->
                                <div class="box-footer">
                                    
                                </div><!-- /.box-footer-->
                            </div><!-- /.box -->
                        </div><!-- /.col -->

                        
</section>  
@stop