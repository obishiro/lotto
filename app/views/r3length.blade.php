@extends('master')

@section('content')
<section class="content-header">
                        <h2 >
                        {{ Lang::get('msg.3lenght', array(), 'th') }}
                        {{ Lang::get('msg.Period', array(), 'th') }}
                        {{ $p->pdate}}
                        

                        </h2>

                        <h3>
                         <small>ยอดสุทธิ</small>
                            <span class="label label-info">{{ number_format(((int) $total )); }}</span>
                         <small>ยอดถูก</small>
                            <span class="label label-danger">{{ number_format(((int) 
                                Session::get('t3')
                             )); }}</span>
                             <small>คงเหลือ</small>
                            <span class="label label-success">{{ number_format(((int) 
                                $total - Session::get('t3')
                             )); }}</span>
                                             </h3>
 
</section>
<section class="content">

		<div class="row">


                        <div class="col-md-12">
                            <!-- Primary box -->
                            <div class="box box-primary">
                                <div class="box-header">
                                     
                                    
                                </div>
                                <div class="box-body">

                                    <table id="table_number" class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                        <th style="width:5%">#</th>
                                        <th >Number</th>
                                        <th>Price</th>
                                        @if(Auth::user()->usertype == 1)
                                        <th>User</th>
                                        @endif
                                        </tr>
                                        </thead>
                                        
                                        <tbody>
                                        @foreach($Number as $key => $n)

                                        <?php
                                        $condition = ['period'=> Session::get('pid'),'number'=>$n->number];
                                         $count = Enumber::where($condition)->count();
                                         $minus =  $p->price - $n->sumprice; ?>
                                        
                                        
                                        
                                         <tr>
                                            <td>
                                                @if($count != 0)

                                            @foreach($enumber as $eid => $e)
                                                @if($e->number == $n->number)
                                                  @if ($e->price - $n->sumprice <=0)
                                                    <span class="label label-danger">
                                                    <i class="fa fa-power-off fa-lg"></i>
                                                </span>
                                                  @endif   
                                                @endif

                                                @endforeach

                                                @else
                                             
                                               @if($minus == "0") 
                                                <span class="label label-warning">
                                                    <i class="fa fa-times-circle fa-lg"></i>
                                                </span>
                                                 @endif
                                                @endif
                                               
                                            
                                             </td>
                                            <td>{{ $n->number }}</td>
                                            <td>

                                                {{ number_format(((int) $n->sumprice )); }}
                                                </td>
                                            @if (Auth::user()->usertype == 1)
                                                <td>
                                                    {{ Auth::user()->name }}
                                                </td>
                                             @endif
                                        </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    
                                      
                                    
                                </div><!-- /.box-body -->
                                <div class="box-footer">
                                    
                                </div><!-- /.box-footer-->
                            </div><!-- /.box -->
                        </div><!-- /.col -->

                        
</section>  
@stop