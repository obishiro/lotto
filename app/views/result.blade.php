
                            <div class="box box-danger">
                                <div class="box-header" >
                                    <h3 class="box-title"></h3>
                                    
                                </div>
                                <div class="box-body">
                                    
  
                                          
                               <div class="box box-solid box-success">
                                <div class="box-header">
                                    <h3 class="box-title"><i class="fa fa-money fa-lg"></i>
                                    {{ Lang::get('msg.Result', array(), 'th')}}
                                    </h3>
                                    
                                </div>
                                <div class="box-body">
                                    @if(Session::has('r2') || Session::has('r3')) 
                                           
                                    <div id="msg-error" class="alert alert-success" role="alert"   style="margin-top:20px;width:98%;">
                                      <i class="fa fa-check-circle fa-2x"></i>
                                      <div class="row">
                                        <div class="col-xs-6">
                                          @if(Session::has('c2'))
                                            มีผู้ถูกหมายเลข
                                              <span class="result">{{ Session::get('r2')}}</span>
                                            จำนวน 
                                                <span class="result">
                                                  {{ Session::get('c2')}}
                                                </span>
                                             คน &nbsp;&nbsp;
                                             <a class="btn btn-info" href="{{ URL::to('home/report/2length')}}" role="button"><i class="fa fa-print"></i> เช็ครายงาน</a>
                                          @endif

                                        </div>
                                        <div class="col-xs-6">
                                          @if(Session::has('c3'))
                                          มีผู้ถูกหมายเลข
                                          <span class="result">
                                            {{ Session::get('r3')}}
                                          </span>
                                          
                                            จำนวน 
                                            <span class="result">{{ Session::get('c3')}}</span>
                                             คน &nbsp;&nbsp;<a class="btn btn-info" href="{{ URL::to('home/report/length')}}" role="button"><i class="fa fa-print"></i> เช็ครายงาน</a>
                                      @endif

                                        </div>
                                      </div>
                                        
                                      
                                      </div>
                                      
                                    @else
                                     <div id="msg-error" class="alert alert-danger" role="alert"   style="margin-top:20px;width:98%;">
                                      <i class="fa fa-power-off fa-2x"></i> <h2>{{ Lang::get('msg.Unresult', array(), 'th') }}  </h2>
                                      </div>
                                      @endif
                                      
                                </div>
                            
                                              </div>
                                </div><!-- /.box-body -->
                                <div class="box-footer">
                                    
                                </div><!-- /.box-footer-->
                                 
                            </div><!-- /.box -->

                       