@extends('master')

@section('content')
<section class="content-header">
<h4 >
                        {{ Lang::get('msg.Sold', array(), 'th') }} {{ $p->pdate}} <br> <br>{{ Lang::get('msg.MaxAllow', array(), 'th') }} <span class="label label-success">{{ $sold }}</span> {{ Lang::get('msg.Total', array(), 'th') }} <span class="label label-info">{{ $total }}</span> {{ Lang::get('msg.Currency', array(), 'th') }} {{ Lang::get('msg.CanSold2', array(), 'th') }}  <span class="label label-danger">{{ $max_allow}}</span> {{ Lang::get('msg.Currency', array(), 'th') }}
                         
                    </h4>
                         
                        
 
</section>
<section class="content">

		<div class="row">


                        <div class="col-md-12">
                            <!-- Primary box -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <form method="POST" id="form_list">
                                    <h3 class="box-title">  {{ Lang::get('msg.List', array(), 'th') }} {{ Session::get('list') }} 
                                        &nbsp;&nbsp;
                                        <button type="button" class="btn btn-info" id="add-newlist">
                                            <i class="fa fa-plus-circle"></i>
                                            {{ Lang::get('msg.Newlist', array(), 'th') }}</button></h3>
                                        <input type="hidden" name="lid" value="{{ Session::get('list') }}">
                                        </form>


                                    
                                </div>
                                <div class="box-body">
                                    <form action="{{ URL::to('home/sold')}}" id="form_sold" method="POST" class="form-inline">
                                    
                                    @for($i=0;$i<=19;$i++)
                                   <div class="row" style="padding-bottom:5px;">
                                    <div class="col-xs-2">
                                        <div class="input-group">
                                            <span class="input-group-addon" id="basic-addon1">เลขที่ {{ $i+1 }} </span>
                                        <input type="text" maxlength="3" name="number[]" class="form-control " id="number-{{$i+1}}" placeholder="" aria-describedby="basic-addon1" >
                                        </div>
                                     </div>
                                     <div class="col-xs-3">
                                         <div class="input-group">
                                            <span class="input-group-addon" id="basic-addon1">จำนวนเงิน</span>
                                        <input type="text" name="price[]" class="form-control " id="price-{{$i+1}}" placeholder="" aria-describedby="basic-addon1" value="">
                                        </div>
                                    </div>
                                    <div class="col-xs-4" id="result-{{ $i+1}}">
                                        
                                    </div>
                                    
                                 </div>
                                 @endfor
                                 <div class="row" style="padding-bottom:5px;">

                                    <div class="col-xs-6">
                                        <div id="msg-error" class="alert alert-danger" role="alert"   style="margin-top:20px;width:80%;display:none">
                             <i class="fa fa-frown-o fa-2x"></i> {{ Lang::get('msg.Error_Max', array(), 'th') }}  <span class="text-danger"> {{ $sold }} </span> {{ Lang::get('msg.Currency', array(), 'th') }} {{ Lang::get('msg.PleaseCheck', array(), 'th') }} 
                                        </div>
                                         <div id="msg-null" class="alert alert-danger" role="alert"   style="margin-top:20px;width:80%;display:none">
                             <i class="fa fa-frown-o fa-2x"></i> {{ Lang::get('msg.Required', array(), 'th') }}   
                                        </div>
                                        <div id="msg-success" class="alert alert-info" role="alert"   style="margin-top:20px;width:80%;display:none">
                             <i class="fa fa-check-circle fa-2x"></i> {{ Lang::get('msg.Success', array(), 'th') }}  
                                        </div>
                                        <button type="button" id="btn-save" class="btn btn-success btn-lg btn-block" style="width:82%"><i class="fa fa-floppy-o fa-lg"></i> {{ Lang::get('msg.Save', array(), 'th') }}</button>
                                      {{--    <button type="submit"  class="btn btn-success btn-lg btn-block" style="width:82%"><i class="fa fa-floppy-o fa-lg"></i> {{ Lang::get('msg.Save', array(), 'th') }}</button> --}}
                                    </div>
                                </div>
                                <input type="hidden" name="list_id" value="{{ Session::get('list') }}">
                                 </form>
                                </div><!-- /.box-body -->
                                <div class="box-footer">
                                    
                                </div><!-- /.box-footer-->
                            </div><!-- /.box -->
                        </div><!-- /.col -->

                        
</section>  
@stop