@extends('master')

@section('content')
<section class="content-header">
<h4 >
                        
                    </h4>
 
</section>
<section class="content">

		<div class="row">


                        <div class="col-md-12">

								

                            <!-- Primary box -->

                            <div class="box box-primary">
                                <div class="box-header" >
                                   <div class="col-md3" style="float:right;margin:5px;">
									 <a class="btn btn-lg  btn-success " style="color:#fff" href="{{ URL::to('/home/adduser')}}" ><i class="fa fa-plus"></i> {{ Lang::get('msg.AddUser',array(),'th')}}</a>
								</div>
                                   
                                </div>
                                <div class="box-body">
                                   
                                                 
                                 <div class="box box-solid box-info">
                                <div class="box-header">
                                    <h3 class="box-title"><i class="fa fa-user fa-lg"></i>
                                    {{ Lang::get('msg.User', array(), 'th')}}
                                    </h3>
                                    
                                </div>
                                <div class="box-body">
                                    <table id="DataUser" class="table table-striped table-bordered">
                                    	<thead>
                                    		<th>#</th>
                                    		<th>{{ Lang::get('msg.User-Name',array(),'th')}}</th>
                                    		<th>{{ Lang::get('msg.User-Username',array(),'th')}}</th>
                                    		<th  width="15%">{{ Lang::get('msg.User-Mobile',array(),'th')}}</th>
                                    		<th  width="25%">{{ Lang::get('msg.User-Address',array(),'th')}}</th>
                                                        <th   width="10%">{{ Lang::get('msg.User-Type',array(),'th')}}</th>
                                                        <th   width="5%">{{ Lang::get('msg.User-Status',array(),'th')}}</th>
                                    		<th width="15%">{{ Lang::get('msg.Tools',array(),'th')}}</th>
                                    	 

                                    	</thead>
                                    </table>



                                </div>
                            
                                </div>
                                
                                </div><!-- /.box-body -->
                                <div class="box-footer">
                                    
                                </div><!-- /.box-footer-->
                            </div><!-- /.box -->
                        </div><!-- /.col -->

                        
</section>  
@stop
@section('script')
<script type="text/javascript">
     var my_table= $('#DataUser').dataTable({
            "bProcessing": true,
            "bServerSide": true,
            "iDisplayLength": 50,
            "targets": 0,
            "sAjaxSource": "{{ $api }}",
            columns: [
            {data:'no',name:'no'},
            {data: 'name', name: 'name'},
            {data: 'userid', name : 'userid'},
            {data: 'mobile', name : 'mobile'},
            
            {data: 'address', name: 'address'},
            {data: 'usertype', name: 'usertype'},
            {data: 'userstatus', name: 'userstatus'},
            
            {data: 'tools', name: 'tools'}
        ],
        "fnDrawCallback":function(){
         table_rows = my_table.fnGetNodes(); 
          $.each(table_rows, function(index){
          $("td:first", this).html(index+1);
          });
         }

            });
</script>
@stop