-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 01, 2016 at 08:21 AM
-- Server version: 5.7.12-0ubuntu1
-- PHP Version: 7.0.4-7ubuntu2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_lotto`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_enumber`
--

CREATE TABLE `tb_enumber` (
  `id` int(11) NOT NULL,
  `number` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `period` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tb_enumber`
--

INSERT INTO `tb_enumber` (`id`, `number`, `price`, `period`) VALUES
(1, '23', '500', '1'),
(2, '37', '500', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tb_number`
--

CREATE TABLE `tb_number` (
  `id` int(11) NOT NULL,
  `number` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` int(25) DEFAULT NULL,
  `period` int(25) DEFAULT NULL,
  `list` int(6) DEFAULT NULL,
  `length` int(6) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `users` int(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_period`
--

CREATE TABLE `tb_period` (
  `id` int(11) NOT NULL,
  `pdate` date DEFAULT NULL,
  `price` int(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tb_period`
--

INSERT INTO `tb_period` (`id`, `pdate`, `price`) VALUES
(1, '2016-05-31', 1000);

-- --------------------------------------------------------

--
-- Table structure for table `tb_result`
--

CREATE TABLE `tb_result` (
  `id` int(11) NOT NULL,
  `r2length` int(6) DEFAULT NULL,
  `r3length` int(6) DEFAULT NULL,
  `period` int(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `userid` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_at` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `usertype` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `userstatus` int(1) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `mobile` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `address` longtext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `userid`, `password`, `remember_token`, `updated_at`, `created_at`, `usertype`, `userstatus`, `name`, `mobile`, `address`) VALUES
(1, 'admin', '$2y$10$gZsxOewUHaPWPnulcn8RluA/GKBFMB.SA9vNlxujbzBMVuC/6yLam', 'wRom3Qyv5tOo7IdYEERcbvENjLlw3AzRp72aXzG2PY3JB6DPeEI56RfSWxif', '2016-06-01 00:00:48', '', '1', 1, 'admin', '0813209321', 'sanom surin'),
(3, 'obishiro', '$2y$10$NJjH1ahUXbCN0bqjevUI9.Pc171khwW3VPbJqBlewpeVVHxhGLemW', 'E1Y67Q6VaqUVuCn5UsDkNWRAqj7rqs9otzHsA2deVqyMG8k4ZSqLG6ACQoWL', '2016-05-31 23:23:42', '2016-05-22 15:30:46', '2', 1, 'Sontaya', '0813209321', 'surin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_enumber`
--
ALTER TABLE `tb_enumber`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_number`
--
ALTER TABLE `tb_number`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_period`
--
ALTER TABLE `tb_period`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_result`
--
ALTER TABLE `tb_result`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_enumber`
--
ALTER TABLE `tb_enumber`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tb_number`
--
ALTER TABLE `tb_number`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_period`
--
ALTER TABLE `tb_period`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tb_result`
--
ALTER TABLE `tb_result`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
